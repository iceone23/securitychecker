package org.securitychecker;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Method;


public class SecurityChecker extends ActionBarActivity implements View.OnClickListener {

    private final int DIALOG_RECOMMEND = 1;

    private boolean wifiEnabled;
    private boolean mobileDataEnabled;
    private boolean gpsEnabled;
    private boolean bluetoothEnabled;
    private boolean airplaneModeEnabled;

    private TextView securityLabel;
    private Button checkSecurityButton;
    private Button recommendButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
    }

    private void checkSecurity() {
        checkWifi();
        checkMobileData();
        checkGPS();
        checkBluetooth();
        checkAirplaneMode();
    }

    private void checkWifi() {
        WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled()){
            wifiEnabled = true;
        } else {
            wifiEnabled = false;
        }
    }

    private void checkMobileData() {
        boolean mobileDataEnabled = false; // Assume disabled
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true); // Make the method callable

            mobileDataEnabled = (Boolean)method.invoke(cm);
        } catch (Exception e) {
            // Some problem accessible private API
        }

        this.mobileDataEnabled = mobileDataEnabled;
    }

    private void checkGPS() {
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            gpsEnabled = true;
        } else {
            gpsEnabled = false;
        }
    }

    private void checkBluetooth() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.isEnabled()) {
            bluetoothEnabled = true;
        } else {
            bluetoothEnabled = false;
        }
    }

    private void checkAirplaneMode() {
        if (isAirplaneModeOn(this)) {
            airplaneModeEnabled = true;
        } else {
            airplaneModeEnabled = false;
        }
    }

    /**
     * Gets the state of Airplane Mode.
     *
     * @param context
     * @return true if enabled.
     */
    private static boolean isAirplaneModeOn(Context context) {

        return Settings.System.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;

    }

    private void initView() {
        securityLabel = (TextView) findViewById(R.id.securityLabel);

        checkSecurityButton = (Button) findViewById(R.id.checkSecurityButton);
        checkSecurityButton.setOnClickListener(this);

        recommendButton = (Button) findViewById(R.id.recommendButton);
        recommendButton.setOnClickListener(this);
        recommendButton.setVisibility(View.INVISIBLE);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.checkSecurityButton:
                checkSecurity();
                updateSecurityLabel();
                recommendButton.setVisibility(View.VISIBLE);

                break;
            case R.id.recommendButton:
                showRecommendMsg();
                break;
        }
    }

    private void updateSecurityLabel() {
        int percent = 100;

        if (wifiEnabled) {
            percent -= 20;
        }

        if (mobileDataEnabled) {
            percent -= 20;
        }

        if (gpsEnabled) {
            percent -= 20;
        }

        if (bluetoothEnabled) {
            percent -= 20;
        }

        if (!airplaneModeEnabled) {
            percent -= 20;
        }

        securityLabel.setText("Security: " + percent + "%");
    }

    private void showRecommendMsg() {

        String recommendMsg = "";

        if (wifiEnabled) {
            recommendMsg += "\nWifi";
        }

        if (mobileDataEnabled) {
            recommendMsg += "\nMobile Data";
        }

        if (gpsEnabled) {
            recommendMsg += "\nGPS";
        }

        if (bluetoothEnabled) {
            recommendMsg += "\nBluetooth";
        }

        if (wifiEnabled || mobileDataEnabled || gpsEnabled || bluetoothEnabled) {
            recommendMsg = "Turn off:" + recommendMsg + "\n\n";
        }

        if (!airplaneModeEnabled) {
            recommendMsg += "Turn on:\nAirplane mode";
        }

        if (!wifiEnabled && !mobileDataEnabled && !gpsEnabled && !bluetoothEnabled && airplaneModeEnabled) {
            recommendMsg += "You are safe!";
        }

        showMessage(recommendMsg);
    }

    /**
     * Показывает короткое всплывающее сообщение
     *
     * @param msg
     *            сообщение
     */
    public void showMessage(String msg) {
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
